import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager
from pages.cockpit_page import CockpitPage
from pages.login_page import LoginPage
from pages.panel_page import PanelPage
from pages.project_page import ProjectPage
from faker import Faker

fake = Faker()


@pytest.fixture()
def browser():
    browser = Chrome(executable_path=ChromeDriverManager().install())
    login_page = LoginPage(browser)
    login_page.load()
    login_page.login("administrator@testarena.pl", "sumXQQ72$L")
    yield browser
    browser.quit()


def test_open_administration(browser):
    cockpit_page = CockpitPage(browser)
    cockpit_page.click_administration()

    assert browser.find_element(By.CSS_SELECTOR, '.content_title').text == 'Projekty'


def test_add_project(browser):
    project_page = ProjectPage(browser)
    project_page.click_add_project()

    assert browser.find_element(By.CSS_SELECTOR, '.content_title').text == 'Dodaj projekt'

    random_name = fake.lexify('????????')
    name_input = browser.find_element(By.CSS_SELECTOR, '[name="name"]')
    name_input.send_keys(random_name)
    return random_name

    random_prefix = fake.company()
    prefix_input = browser.find_element(By.CSS_SELECTOR, '[name="prefix"]')
    prefix_input.send_keys(random_prefix)

    project_page = ProjectPage(browser)
    project_page.click_save_project()

    assert browser.find_element(By.CSS_SELECTOR, '.content_label_title').text == random_name


def test_search_project(browser, random_name):
    project_page = ProjectPage(browser)
    project_page.search_projects()

    assert browser.find_element(By.CSS_SELECTOR, '.filterButton')

    searching_name = test_add_project.random_name
    search_input = browser.find_element(By.CSS_SELECTOR, '[name="search"]')
    search_input.send_keys(searching_name)

    search_button = browser.find_element(By.CSS_SELECTOR, '#j_searchButton').click()

    # assert nie wiem jaki selktor dla <a href="http://demo.testarena.pl/administration/project_view/4745">krylllllll</a>
