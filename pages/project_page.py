from faker import Faker
fake = Faker()
from selenium.webdriver.common.by import By


class ProjectPage:
    def __init__(self, browser):
        self.browser = browser

    def click_add_project(self):
        add_project_button = self.browser.find_elements(By.CSS_SELECTOR, '.button_link')[0]
        add_project_button.click()

    def click_save_project(self, random_name):
        save_project_button = self.browser.find_elements(By.CSS_SELECTOR, '#save input')
        save_project_button.click()

    def search_projects(self):
        projects_button = self.browser.find_elements(By.CSS_SELECTOR, '.activeMenu')
        projects_button.click()